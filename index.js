/*
	Activity:

	1. Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
	2. Create a class constructor able to receive 3 arguments
		-It should be able to receive two strings and a number
		-Using the this keyword assign properties:
			name, 
			breed, 
			dogAge = <7x human years> 
			-assign the parameters as values to each property.

	Create 2 new objects using our class constructor.

	This constructor should be able to create Dog objects.

	Log the 2 new Dog objects in the console.


*/

let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
}
 introduce(student1);

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}
 introduce(student2);

class Dog {
	constructor(name, breed, dogAge){
		this.name = name;
		this.breed = breed;
		this.age = dogAge;
	}
} //END .Dog

let dog1 = new Dog("Spot", "Askal", "5");
console.log(dog1);
let dog2 = new Dog("Bruno", "Rottweiler", "2");
console.log(dog2);

function introduce(student1){
	//Note: You can destructure objects inside functions.
	let intro1 = console.log(`Hi! I'm ${student1.name}. I am ${student1.age} years old.`);
	let intro2 = console.log(`I study the following courses ${student1.classes[0]} and ${student1.classes[1]}.`);
}//End introduce student 1
function introduce(student2){

	let intro3 = console.log(`Hi! I'm ${student2.name}. I am ${student2.age} years old.`);
	let intro4 = console.log(`I study the following courses ${student2.classes[0]} and ${student2.classes[1]}.`);
}//End introduce student 2


const getCube = (num) => { 
		let threePowerOf3 = console.log(`The cube of ${num} is ${num**3}`);
}

let cube = getCube(3);

/*let numArr = [15,16,32,21,21,2];*/

const numArr = [15,16,32,21,21,2];

numArr.forEach(testFunction = (num) =>{


	console.log(num);
})

numsSquared = numArr.map(testfunction = (num) => {


	return num ** 2;

})

console.log(numSquared);
